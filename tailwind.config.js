const plugin = require('tailwindcss/plugin')

/**
 * Tailwind css configuration file
 *
 * Add some configuration for class generated in tailwind
 * classes
 */
module.exports = {
  content: [
    './components/**/*.{js,vue,ts}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
  ],
  theme: {
    fontFamily: {
      body: ['Lato', 'sans-serif'],
    },
    extend: {
      colors: {
        primary: {
          blue: '#3182CE',
          green: '#319795',
        },
        accent: {
          cyan: '#81E6D9',
          'light-cyan': '#E6FFFA',
        },
        dark: {
          300: '#718096',
          400: '#4A5568',
          500: '#2D3748',
        },
      },
    },
  },
  plugins: [
    plugin(function ({ addUtilities }) {
      addUtilities({
        '.no-scrollbar': {
          '-ms-overflow-style': 'none',
          'scrollbar-width': 'none',
        },
        '.no-scrollbar::-webkit-scrollbar': {
          display: 'none',
        },
      })
    }),
  ],
}
