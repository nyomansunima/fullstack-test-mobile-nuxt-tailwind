const open = require('open');

/**
 * # Nuxt JS Configuration file
 *
 * contain all of the website configuration in each of element
 * to define some folder, structure, meta data, and so on
 */
export default {
  // The Global Application setting
  // add something like the initial data for the website like meta tags
  // see detail https://go.nuxtjs.dev/config-head
  head: {
    title: 'Mobile View - The complte simple Mobile View Fullstack Developer',
    htmlAttrs: {
      lang: 'en',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'The complte simple Mobile View Fullstack Developer',
      },
      {
        hid: 'keywords',
        name: 'keywords',
        content: 'Mobile, View, Fullstack, Developer, Nuxt Js, Tailwind, Css',
      },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  // If you wish to used another css for global
  // you can add it here, See all detail https://go.nuxtjs.dev/config-css
  css: ['@/assets/css/main.css'],

  // Add some plugin to run before the site has been rendered
  // super cool see detail https://go.nuxtjs.dev/config-plugins
  plugins: [],

  // This will make sure to auto input component
  // set to false to make it manual, and see detail https://go.nuxtjs.dev/config-components
  components: true,

  // Build module configuration
  // see detail https://go.nuxtjs.dev/config-modules
  buildModules: ['@nuxt/postcss8', '@nuxt/typescript-build'],

  // Add some module to the application
  // see detail https://go.nuxtjs.dev/config-modules
  modules: [],

  // Add some configuration when building the project
  // see detail https://go.nuxtjs.dev/config-build
  build: {
    postcss: {
      plugins: {
        tailwindcss: {},
        autoprefixer: {},
      },
    },
  },
  hooks: {
    listen(server, { host, port }) {
      open(`http://${host}:${port}`)
    }
  }
}
